const child_process = require('child_process')
const fs = require('fs')
const path = require('path')

const AWS = require('aws-sdk')
const inquirer = require('inquirer')

const AWSRegions = {
  'us-east-1': 'USA East (North Virginia)',
  'us-east-2': 'USA East (Ohio)',
  'us-west-1': 'USA West (North California)',
  'us-west-2': 'USA West (Oregon)',
  'ap-south-1': 'Pacific Asia (Mumbai)',
  'ap-northeast-2': 'Pacific Asia (Seoul)',
  'ap-southeast-1': 'Pacific Asia (Singapore)',
  'ap-southeast-2': 'Pacific Asia (Sydney)',
  'ap-northeast-1': 'Pacific Asia (Tokyo)',
  'ca-central-1': 'Canada (Central)',
  'eu-central-1': 'UE (Frankfort)',
  'eu-west-1': 'UE (Ireland)',
  'eu-west-2': 'UE (London)',
  'eu-west-3': 'UE (Paris)',
  'sa-east-1': 'South America (São Paulo)',
}

main().catch(err => console.error(err))

async function main () {

  if (process.argv.indexOf('--help') > -1) {
    return printHelp()
  }

  if (process.argv.indexOf('--reset-all') > -1) {
    resetAll()
  }

  if (process.argv.indexOf('--reset-credentials') > -1) {
    await resetCredentials()
  }

  if (process.argv.indexOf('--reset-key-path') > -1) {
    resetKeyPath().catch(err => console.error(err))
  }

  try {
    const config = await getConfig()

    AWS.config.credentials = new AWS.Credentials(config.accessKeyId, config.secretAccessKey)

    const region = await askForRegion()

    AWS.config.update({
      region: region.region
    })

    const ec2 = new AWS.EC2({apiVersion: 'latest'})

    const instances = await getInstances(ec2)
    const answer = await askForInstance(instances)

    const instance = instances.filter(instance => {
      return instance.name === answer.instance
    })

    const key = await askForKey(instance[0].key)

    runSSH(instance[0].ip, key.key)
  } catch (err) {
    console.error(err)
  }
}

async function getConfig () {
  const configDirectory = `${getUserHome()}/.aws-ssh-client`
  const configFile = `${configDirectory}/config.json`
  if (fs.existsSync(configFile)) {
    return JSON.parse(fs.readFileSync(configFile, 'utf-8'))
  } else {
    const config = Object.assign({}, await askForCredentials(), await askForKeyPath())
    if (!fs.existsSync(configDirectory)) {
      fs.mkdirSync(configDirectory)
    }
    fs.writeFileSync(configFile, JSON.stringify(config), 'utf-8')
    return config
  }
}

function askForRegion () {
  const choices = Object.keys(AWSRegions).map((key) => {
    return {
      value: key,
      name: `[${key}] ${AWSRegions[key]}`
    }
  })

  return inquirer.prompt([{
    type: 'list',
    name: 'region',
    message: 'Choose your region',
    choices: choices
  }])
}

function getInstances (ec2) {
  return new Promise((resolve, reject) => {
    const ec2instances = []
    ec2.describeInstances((err, instances) => {
      if (err) {
        reject(reject)
      }

      if (Array.isArray(instances.Reservations)) {
        instances.Reservations.forEach(reservation => {
          reservation.Instances.forEach((instance) => {
            if (instance.State.Name !== 'running') {
              return
            }

            const instanceName = instance.Tags.filter(tag => {
              return tag.Key === 'elasticbeanstalk:environment-name'
            })[0].Value

            const keyName = instance.KeyName

            ec2instances.push({
              ip: instance.PublicDnsName,
              name: instanceName,
              key: keyName
            })
          })

          resolve(ec2instances)
        })
      }
    })
  })
}

function askForKey (prop) {
  return inquirer.prompt([{
    type: 'list',
    name: 'key',
    message: `Choose your key: (${prop})`,
    choices: walkSync(`${getUserHome()}/.ssh`)
  }])
}

function askForInstance (instances) {
  const choices = instances.map(instance => {
    return instance.name
  })
  return inquirer.prompt([{
    type: 'list',
    name: 'instance',
    message: 'Choose your instance',
    choices: choices
  }])
}

function askForCredentials () {
  return inquirer.prompt([{
    type: 'input',
    name: 'accessKeyId',
    message: 'Enter your access key id'
  }, {
    type: 'input',
    name: 'secretAccessKey',
    message: 'Enter your secret access key'
  }])
}

function askForKeyPath () {
  return inquirer.prompt([{
    type: 'input',
    name: 'keyPath',
    message: 'Select your ssh key directory',
    default: `${getUserHome()}/.ssh`
  }])
}

function walkSync (dir, fileList) {
  const files = fs.readdirSync(dir)
  fileList = fileList || []
  files.forEach((file) => {
    if (fs.statSync(path.resolve(dir, file)).isDirectory()) {
      fileList = walkSync(path.resolve(dir, file), fileList)
    } else {
      fileList.push(path.resolve(dir, file))
    }
  })
  return fileList
}

function getUserHome () {
  return process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
}

function runSSH (host, key) {
  child_process.spawn('ssh', [
    '-i', key, `ec2-user@${host}`
  ], {
    stdio: 'inherit'
  })
}

function printHelp () {
  const style = {
    Reset: "\x1b[0m",
    Bright: "\x1b[1m",
    Dim: "\x1b[2m",
    Underscore: "\x1b[4m",
    Blink: "\x1b[5m",
    Reverse: "\x1b[7m",
    Hidden: "\x1b[8m",

    FgBlack: "\x1b[30m",
    FgRed: "\x1b[31m",
    FgGreen: "\x1b[32m",
    FgYellow: "\x1b[33m",
    FgBlue: "\x1b[34m",
    FgMagenta: "\x1b[35m",
    FgCyan: "\x1b[36m",
    FgWhite: "\x1b[37m",

    BgBlack: "\x1b[40m",
    BgRed: "\x1b[41m",
    BgGreen: "\x1b[42m",
    BgYellow: "\x1b[43m",
    BgBlue: "\x1b[44m",
    BgMagenta: "\x1b[45m",
    BgCyan: "\x1b[46m",
    BgWhite: "\x1b[47m",
  }
  process.stdout.write(`${style.Underscore}How to use:${style.Reset}
Just type:
${style.FgGreen}./aws-client [options]${style.Reset}

Options:
  ${style.FgGreen}--help${style.Reset}                Print this help
  ${style.FgGreen}--reset-all${style.Reset}           Erase ~/.aws-ssh-client/config.json and re-ask you all the questions
  ${style.FgGreen}--reset-credentials${style.Reset}   Re-ask you for credentials
  ${style.FgGreen}--reset-key-path${style.Reset}      Re-ask you for ssh key path
  
${style.Reset}
`)
}

function resetAll () {
  const configDirectory = `${getUserHome()}/.aws-ssh-client`
  const configFile = `${configDirectory}/config.json`
  if (fs.existsSync(configFile)) {
    fs.unlinkSync(configFile)
  }
}

async function resetCredentials () {
  const configDirectory = `${getUserHome()}/.aws-ssh-client`
  const configFile = `${configDirectory}/config.json`
  let config = JSON.parse(fs.readFileSync(configFile, 'utf-8'))
  config = Object.assign(config, await askForCredentials())
  fs.writeFileSync(configFile, JSON.stringify(config), 'utf-8')
  return true
}

async function resetKeyPath () {
  const configDirectory = `${getUserHome()}/.aws-ssh-client`
  const configFile = `${configDirectory}/config.json`
  let config = JSON.parse(fs.readFileSync(configFile, 'utf-8'))
  config = Object.assign(config, await askForKeyPath())
  fs.writeFileSync(configFile, JSON.stringify(config), 'utf-8')
  return true
}
