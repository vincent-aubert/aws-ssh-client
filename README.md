# AWS SSH CLIENT

Aws ssh client is a tool that allow you to connect to your EC2 instance without going to the AWS console to get the public DNS.

### Installation

First, clone this repository.

Then, type in your console :

```
npm i
```

And last
```
node index.js
```

### How to use
You'll need a _access key id_ and _secret access key_ to log you to AWS. You can generate it with the IAM service.

The first time you'll launch this app, you need to register the _access key_ id and the _secret access key_.

The second step is to register the directory where are stored your ssh keys to connect to EC2 instances (usually in _~/.ssh/_).

A JSON file will be created : `~/.aws-ssh-client/config.json`. This file contains the previous information, so you don't need to save them each time you launch this app.

If you want to reset the configuration, these are three commands :

__To reset all configuration__
```
node index.js --reset-all
```

__To reset only credentials__
```
node index.js --reset-credentials
```

__To reset only ssh key path__
```
node index.js --reset-key-path
```

__To get more information__
```
node index.js --help
```

If you use the packaged version, replace `node index.js` by `aws-client` (or the name you give it).

### Build packages

To build the executable packages, you need to install __[pkg](https://github.com/zeit/pkg)__

The packages destination is the _dist/_ directory.

```
npm i -g pkg
```

#### Build for all targets
```
npm run build
```

#### Build for MacOS
```
npm run build-mac
```

#### Build for Linux
```
npm run build-linux
```

#### Build for Windows
```
npm run build-win
```

## Licence

Do what you want with this code
